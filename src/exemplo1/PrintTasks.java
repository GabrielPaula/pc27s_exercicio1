/**
 * Exemplo1: Programacao com threads
 * Autor: Lucio Agostinho Rocha
 * Ultima modificacao: 07/08/2017
 */
package exemplo1;

import java.util.Random;

/**
 *
 * @author Gabriel Souza de Paula
 */
public class PrintTasks implements Runnable {

    private final int sleepTime; //tempo de adormecimento aleatorio para a thread
    private final String taskName; //nome da tarefa
    private final static Random generator = new Random();
    private Integer ID;

    public PrintTasks(String name, Integer identificadorThread){
        taskName = name;
        this.ID = identificadorThread;
        //Tempo aleatorio entre 0 e 5 segundos
        sleepTime = generator.nextInt(1000); //milissegundos
    }
    
    public void run(){
        try{
            if(ID % 2 != 0){

            System.out.printf("Tarefa: %s dorme por %d ms\n", taskName, sleepTime);
            //Estado de ESPERA SINCRONIZADA
            //Nesse ponto, a thread perde o processador, e permite que
            //outra thread execute
            Thread.sleep(sleepTime);
            }
        } catch (InterruptedException ex){
            System.out.printf("%s %s\n", taskName, "terminou de maneira inesperada.");
        }
       System.out.printf("%s acordou!\n", taskName);
    }
       
}
