/**
 * Exemplo1: Programacao com threads
 * Autor: Lucio Agostinho Rocha
 * Ultima modificacao: 07/08/2017
 */
package exemplo1;

import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 *
 * @author Gabriel Souza de Paula
 */
public class Exemplo1  {

        public static void main(String [] args){
            
            System.out.println("Inicio da criacao das threads.");

            //Cria cada thread com um novo runnable selecionado

            ExecutorService threadExecutor = Executors.newCachedThreadPool();
            for (int i=1; i<=20; i++){
                Thread t20s = new Thread(new PrintTasks("thread " + i,i));
                threadExecutor.execute(t20s);
            }
            threadExecutor.shutdown();
            
            System.out.println("Threads criadas");
        }
        
}
